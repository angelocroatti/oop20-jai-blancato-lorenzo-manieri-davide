package model;

public interface MovableXY extends Movable{

	public void moveUp(); 
	
	public void moveDown(); 
	
	public void moveLeft(); 
	
	public void moveRight(); 
	
}

